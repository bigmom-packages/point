<?php

namespace Bigmom\Point\Contracts;

interface Deconstructor
{
    public function one(string $input): array;

    public function all(array $input): array;
}
