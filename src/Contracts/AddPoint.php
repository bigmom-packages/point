<?php

namespace Bigmom\Point\Contracts;

use Bigmom\Point\Objects\Status;
use Illuminate\Database\Eloquent\Model;

interface AddPoint
{
    public function add(Model $owner, int $value, array $tags): Status;

    public function addWithoutModel(int $ownerId, string $ownerType, int $value, array $tags): Status;
}