<?php

namespace Bigmom\Point\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Bigmom\Point\Objects\PointResult;

interface GetPoint
{
    public function getLimitedSum(Model $owner, $tags = null): int;

    public function getLimitedSumWithoutModel(int $ownerId, string $ownerType, array $tags = null): int;

    public function get(Model $owner, $tags = null): PointResult;

    public function getWithoutModel(int $ownerId, string $ownerType, array $tags = null): PointResult;
}
