<?php

namespace Bigmom\Point\Objects;

class PointResult
{
    protected $result;
    protected $sum;
    protected $count;
    protected $limit;
    protected $limitedSum;

    public function __construct(array $result, string $sum, int $count, int $limit = null)
    {
        $this->result = $result;
        $this->sum = $sum;
        $this->count = $count;
        $this->limit = $limit;
        if ($limit === null) {
            $limitedSum = $sum;
        } else {
            $limitedSum = $sum > $limit ? $limit : $sum;
        }
        $this->limitedSum = $limitedSum;
    }

    public function all()
    {
        return get_object_vars($this);
    }

    public function result()
    {
        return $this->result;
    }

    public function sum()
    {
        return $this->sum;
    }

    public function count()
    {
        return $this->count;
    }

    public function limit()
    {
        return $this->limit;
    }

    public function limitedSum()
    {
        return $this->limitedSum;
    }
}
