<?php

namespace Bigmom\Point\Objects;

class Status
{
    protected $code;
    protected $message;
    protected $extra;

    public function __construct(int $code, string $message, $extra = null)
    {
        $this->code = $code;
        $this->message = $message;
        $this->extra = $extra;
    }

    public function all()
    {
        return get_object_vars($this);
    }

    public function code()
    {
        return $this->code;
    }

    public function message()
    {
        return $this->message;
    }

    public function extra()
    {
        return $this->extra;
    }
}
