<?php

namespace Bigmom\Point\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;

    public function owner()
    {
        return $this->morphTo();
    }

    public function tags()
    {
        return $this->hasMany(PointTag::class);
    }
}
