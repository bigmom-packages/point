<?php

namespace Bigmom\Point\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointTag extends Model
{
    use HasFactory;

    public function point()
    {
        return $this->belongsTo(Point::class);
    }
}
