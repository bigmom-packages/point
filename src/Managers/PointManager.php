<?php

namespace Bigmom\Point\Managers;

use Illuminate\Database\Eloquent\Model;
use Bigmom\Point\Contracts\AddPoint;
use Bigmom\Point\Contracts\GetPoint;
use Bigmom\Point\Objects\PointResult;
use Bigmom\Point\Objects\Status;

class PointManager
{
    protected $addPointService;

    protected $getPointService;

    public function __construct(AddPoint $addPointService, GetPoint $getPointService)
    {
        $this->addPointService = $addPointService;
        $this->getPointService = $getPointService;
    }

    public function add(Model $owner, int $value = 1, array $tags): Status
    {
        return $this->addPointService->add($owner, $value, $tags);
    }

    public function addWithoutModel(int $ownerId, string $ownerType, int $value = 1, array $tags): Status
    {
        return $this->addPointService->addWithoutModel($ownerId, $ownerType, $value, $tags);
    }

    public function getLimitedSum(Model $owner, array $tags = null): int
    {
        return $this->getPointService->getLimitedSum($owner, $tags);
    }

    public function getLimitedSumWithoutModel(int $ownerId, string $ownerType, array $tags = null): int
    {
        return $this->getPointService->getLimitedSumWithoutModel($ownerId, $ownerType, $tags);
    }
    
    public function get(Model $owner, array $tags = null): PointResult
    {
        return $this->getPointService->get($owner, $tags);
    }

    public function getWithoutModel(int $ownerId, string $ownerType, array $tags = null): PointResult
    {
        return $this->getPointService->getWithoutModel($ownerId, $ownerType, $tags);
    }
}
