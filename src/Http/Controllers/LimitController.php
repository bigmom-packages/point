<?php

namespace Bigmom\Point\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Bigmom\Point\Models\Limit;

class LimitController extends Controller
{
    public function getIndex(Request $request)
    {
        $limits = Limit::paginate(15);

        return view('bigmom-point::limit.index', compact('limits'));
    }

    public function postCreate(Request $request)
    {
        $request->validate([
            'tag' => ['required', 'string', 'max:191', 'unique:limits,tag'],
            'value' => ['required', 'integer', 'min:1', 'max:'.PHP_INT_MAX],
        ]);

        $limit = new Limit;
        $limit->tag = $request->input('tag');
        $limit->value = $request->input('value');
        $limit->save();

        return back()
            ->with('success', 'Limit successfully created.');
    }

    public function postUpdate(Limit $limit, Request $request)
    {
        $request->validate([
            'value' => ['required', 'integer', 'min:1', 'max:'.PHP_INT_MAX],
        ]);

        $limit->value = $request->input('value');
        $limit->save();

        return back()
            ->with('success', 'Limit successfully updated.');
    }

    public function postDelete(Limit $limit)
    {
        $limit->delete();
        
        return back()
            ->with('success', 'Limit successfully deleted.');
    }
}
