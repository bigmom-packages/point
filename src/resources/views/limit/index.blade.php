<x-bigmom-auth::layout.main>
    @push('script')
    <script src="{{ asset('/vendor/bigmom-point/js/point.js') }}"></script>
    @endpush

    <x-slot name="header">Point Limit</x-slot>
    <x-slot name="headerRightSide">
        <x-bigmom-auth::button.link.blue href="{{ route('bigmom-auth.getHome') }}">Home</x-bigmom-auth::button.link.blue>
    </x-slot>

    <x-bigmom-auth::card class="pt-8">
        <div class="flex justify-end my-2">
            <x-bigmom-auth::button.blue class="mx-2" data-micromodal-trigger="modal-create">Create</x-pobigmom-authll::button.blue>
        </div>
        <table class="w-full table-auto border-collapse mb-2">
            <thead>
                <tr>
                    <x-bigmom-auth::table.th>Tag</x-bigmom-auth::table.th>
                    <x-bigmom-auth::table.th>Value</x-bigmom-auth::table.th>
                    <x-bigmom-auth::table.th>Actions</x-bigmom-auth::table.th>
                </tr>
            </thead>
            <tbody>
                @forelse ($limits as $limit)
                <tr>
                    <x-bigmom-auth::table.td>{{ $limit->tag }}</x-bigmom-auth::table.td>
                    <x-bigmom-auth::table.td>{{ $limit->value }}</x-bigmom-auth::table.td>
                    <x-bigmom-auth::table.td>
                        <x-bigmom-auth::button.yellow data-micromodal-trigger="modal-update" class="btn-update mx-auto my-1"
                            data-route="{{ route('bigmom-point.limit.postUpdate', $limit) }}"
                            data-value="{{ $limit->value }}">
                                Update</x-bigmom-auth::button.yellow>
                        <x-bigmom-auth::button.red data-micromodal-trigger="modal-delete" class="btn-delete mx-auto" data-route="{{ route('bigmom-point.limit.postDelete', $limit) }}" data-tag="{{ $limit->tag }}">
                            Delete
                        </x-bigmom-auth::button.red>
                    </x-bigmom-auth::table.td>
                </tr>
                @empty
                <tr>
                    <td class="pt-2">
                       No limits yet! Click on the blue "Create" button to create one.
                    </td>
                </tr> 
                @endforelse
            </tbody>
        </table>
        {{ $limits->links() }}
    </x-bigmom-auth::card>

    @push('modal')
    <div id="modal-create" aria-hidden="true" class="modal fixed top-0 left-0 w-screen h-screen flex justify-center items-center">
        <div tabindex="-1" data-micromodal-close class="w-full h-full bg-gray-600 opacity-50 absolute top-0 left-0"></div>
        <div role="dialog" aria-model="true" aria-labelledby="modal-create-title" class="relative bg-white container z-30 rounded-lg px-8 py-8 overflow-auto" style="max-height: 75%">
            <header class="py-2">
                <h2 id="modal-create-title" class="font-bold text-2xl text-blue-900">
                    Create Limit
                </h2>
            </header>
            <div id="modal-create-content" class="mb-8">
                <form method="POST" action="{{ route('bigmom-point.limit.postCreate') }}">
                    @csrf
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::form.label for="create-tag">Tag</x-bigmom-auth::form.label>
                        <x-bigmom-auth::form.input id="create-tag" type="text" name="tag" value="{{ old('tag') }}" required></x-bigmom-auth::form.input>
                    </x-bigmom-auth::form.section>
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::form.label for="create-value">Value</x-bigmom-auth::form.label>
                        <x-bigmom-auth::form.input id="create-value" type="text" name="value" value="{{ old('value') }}" required></x-bigmom-auth::form.input>
                    </x-bigmom-auth::form.section>
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::button.blue type="submit">Create</x-bigmom-auth::button.blue>
                    </x-bigmom-auth::form.section>
                </form>
            </div>
        </div>
    </div>
    <div id="modal-update" aria-hidden="true" class="modal fixed top-0 left-0 w-screen h-screen flex justify-center items-center">
        <div tabindex="-1" data-micromodal-close class="w-full h-full bg-gray-600 opacity-50 absolute top-0 left-0"></div>
        <div role="dialog" aria-model="true" aria-labelledby="modal-update-title" class="relative bg-white container max-h-3/4 z-30 rounded-lg px-8 py-8 overflow-auto">
            <header class="py-2">
                <h2 id="modal-update-title" class="font-bold text-2xl text-blue-900">
                    Update Limit
                </h2>
            </header>
            <div id="modal-update-content" class="mb-8">
                <form method="POST" action="" id="update-form">
                    @csrf
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::form.label for="update-value">Value</x-bigmom-auth::form.label>
                        <x-bigmom-auth::form.input id="update-value" type="text" name="value" required></x-bigmom-auth::form.input>
                    </x-bigmom-auth::form.section>
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::button.yellow type="submit">Update</x-bigmom-auth::button.yellow>
                    </x-bigmom-auth::form.section>
                </form>
            </div>
        </div>
    </div>
    <div id="modal-delete" aria-hidden="true" class="modal fixed top-0 left-0 w-screen h-screen flex justify-center items-center">
        <div tabindex="-1" data-micromodal-close class="w-full h-full bg-gray-600 opacity-50 absolute top-0 left-0"></div>
        <div role="dialog" aria-model="true" aria-labelledby="modal-delete-title" class="relative bg-white container max-h-3/4 z-30 rounded-lg px-8 py-8 overflow-auto">
            <header class="py-2">
                <h2 id="modal-delete-title" class="font-bold text-2xl text-blue-900">
                    Are you sure you want to delete this limit and all its children?
                </h2>
            </header>
            <div id="modal-delete-content">
                <form id="delete-form" method="POST" action="">
                    @csrf
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::form.label id="delete-tag"></x-bigmom-auth::form.label>
                    </x-bigmom-auth::form.section>
                    <x-bigmom-auth::form.section>
                        <x-bigmom-auth::button.red type="submit">Delete</x-bigmom-auth::button.red>
                    </x-bigmom-auth::form.section>
                </form>
            </div>
        </div>
    </div>
    @endpush
</x-bigmom-auth::layout.main>
