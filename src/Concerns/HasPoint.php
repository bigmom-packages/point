<?php

namespace Bigmom\Point\Concerns;

use Bigmom\Point\Models\Point;

trait HasPoint
{
    public function points()
    {
        return $this->morphMany(Point::class, 'owner');
    }
}
