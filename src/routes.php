<?php

use Illuminate\Support\Facades\Route;
use Bigmom\Point\Http\Controllers\LimitController;
use Bigmom\Auth\Http\Middleware\Authenticate;
use Bigmom\Auth\Http\Middleware\EnsureUserIsAuthorized;

Route::prefix('/bigmom/point')
    ->name('bigmom-point.')
    ->middleware(['web', Authenticate::class, EnsureUserIsAuthorized::class.':point-limit-manage'])
    ->group(function () {
        Route::prefix('/limit')->name('limit.')->group(function () {
            Route::get('/', [LimitController::class, 'getIndex'])->name('getIndex');
            Route::post('/create', [LimitController::class, 'postCreate'])->name('postCreate');
            Route::post('/{limit}/update', [LimitController::class, 'postUpdate'])->name('postUpdate');
            Route::post('/{limit}/delete', [LimitController::class, 'postDelete'])->name('postDelete');
        });
});
