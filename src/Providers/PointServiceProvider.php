<?php

namespace Bigmom\Point\Providers;

use Illuminate\Support\ServiceProvider;
use Bigmom\Point\Managers\PointManager;
use Bigmom\Point\Services\DotNotationDeconstructor;
use Bigmom\Point\Services\InternalAddPoint;
use Bigmom\Point\Services\InternalGetPoint;

class PointServiceProvider extends ServiceProvider
{
    public function register()
    {
        config([
            'bigmom-auth.packages' => array_merge([[
                'name' => 'Point',
                'description' => 'To manage points',
                'routes' => [
                    [
                        'title' => 'Manage limits',
                        'name' => 'bigmom-point.limit.getIndex',
                        'permission' => 'point-limit-manage',
                    ]
                ],
                'permissions' => [
                    'point-limit-manage',
                ]
            ]], config('bigmom-auth.packages', []))
        ]);

        if (config('bigmom-point.use-provided-facade')) {
            $this->app->singleton('bigmom-point', function ($app) {
                $deconstructor = new DotNotationDeconstructor;
                return new PointManager(new InternalAddPoint($deconstructor), new InternalGetPoint($deconstructor));
            });
        }
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__.'/../migrations');

            $this->publishes([
                __DIR__.'/../config/bigmom-point.php' => config_path('bigmom-point.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/bigmom-point'),
            ], 'public');
        }

        if (config('bigmom-point.use-provided-cms')) {
            $this->loadRoutesFrom(__DIR__.'/../routes.php');

            $this->loadViewsFrom(__DIR__.'/../resources/views', 'bigmom-point');
        }
    }
}
