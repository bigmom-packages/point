<?php

namespace Bigmom\Point\Facades;

use Illuminate\Support\Facades\Facade;

class Point extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'bigmom-point';
    }
}
