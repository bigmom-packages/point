<?php

namespace Bigmom\Point\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Bigmom\Point\Contracts\Deconstructor;
use Bigmom\Point\Contracts\GetPoint;
use Bigmom\Point\Models\Limit;
use Bigmom\Point\Models\Point;
use Bigmom\Point\Objects\PointResult;

class InternalGetPoint implements GetPoint
{
    protected $deconstructor;

    public function __construct(Deconstructor $deconstructor)
    {
        $this->deconstructor = $deconstructor;
    }

    public function getLimitedSum(Model $owner, $tags = null): int
    {
        return $this->getLimitedSumWithoutModel($owner->id, get_class($owner), $tags);
    }

    public function getLimitedSumWithoutModel(int $ownerId, string $ownerType, ?array $tags = null): int
    {
        return $this->getWithoutModel($ownerId, $ownerType, $tags)->limitedSum();
    }

    public function get(Model $owner, $tags = null): PointResult
    {
        return $this->getWithoutModel($owner->id, get_class($owner), $tags);
    }

    public function getWithoutModel(int $ownerId, string $ownerType, array $tags = null, $page = 1): PointResult
    {
        if ($tags !== null && count($tags) !== 0) {
            $limit = Limit::whereIn('tag', $this->deconstructor->all($tags))->min('value');
        } else {
            $limit = null;
        }

        $query = $this->buildQuery($ownerId, $ownerType, $tags);
        $points = $query->simplePaginate(15, ['*'], 'page', $page)->items();
        $sum = $query->sum('value');
        $count = $query->count('value');

        return new PointResult($points, $sum, $count, $limit);
    }

    private function buildQuery(int $ownerId, string $ownerType, array $tags = null): Builder
    {
        return Point::where('owner_id', $ownerId)
            ->where('owner_type', $ownerType)
            ->when($tags, function ($query, $tags) {
                foreach ($tags as $tag) {
                    $query = $query->whereHas('tags', function ($query) use ($tag) {
                        return $query->where('tag', $tag);
                    });
                }
                return $query;
            });
    }
}
