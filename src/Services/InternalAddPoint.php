<?php

namespace Bigmom\Point\Services;

use Bigmom\Point\Facades\Point as FacadesPoint;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Bigmom\Point\Contracts\AddPoint;
use Bigmom\Point\Contracts\Deconstructor;
use Bigmom\Point\Models\Limit;
use Bigmom\Point\Models\Point;
use Bigmom\Point\Models\PointTag;
use Bigmom\Point\Objects\Status;

class InternalAddPoint implements AddPoint
{
    protected $deconstructor;

    public function __construct(Deconstructor $deconstructor)
    {
        $this->deconstructor = $deconstructor;
    }

    public function add(Model $owner, int $value = 1, array $tags): Status
    {
        return $this->addWithoutModel($owner->id, get_class($owner), $value, $tags);
    }

    public function addWithoutModel(int $ownerId, string $ownerType, int $value = 1, array $tags): Status
    {
        $this->validateTags($tags);

        $canProceed = true;

        foreach ($tags as $tag) {
            $limit = Limit::whereIn('tag', $this->deconstructor->one($tag))->min('value');

            if ($limit !== null) {
                if (FacadesPoint::getWithoutModel($ownerId, $ownerType, [$tag])->sum() >= $limit) {
                    $canProceed = false;
                    break;
                };
            }
        }

        if ($canProceed) {
            $point = DB::transaction(function () use ($ownerId, $ownerType, $value, $tags) {
                $point = new Point;
                $point->owner_id = $ownerId;
                $point->owner_type = $ownerType;
                $point->value = $value;
                $point->save();

                foreach ($tags as $tag) {
                    $pointTag = new PointTag();
                    $pointTag->point_id = $point->id;
                    $pointTag->tag = $tag;
                    $pointTag->save();
                }

                return $point;
            });

            return $point->id 
                ? new Status(0, "Point added successfully.", $point)
                : new Status(1, "An error has occured.");
        } else {
            return new Status(2, "Limit reached.");
        }
    }

    private function validateTags(array $tags)
    {
        if (count($tags) === 0) {
            throw new Exception("Invalid number of tags provided.");
        }

        foreach ($tags as $tag)
        {
            if (! is_string($tag)) {
                throw new Exception("Tag is not string.");
            } else if (strlen($tag) > 191) {
                throw new Exception("Tag is more than 191 characters.");
            }
        }
    }
}
