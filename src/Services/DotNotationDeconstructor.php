<?php

namespace Bigmom\Point\Services;

use Bigmom\Point\Contracts\Deconstructor;

class DotNotationDeconstructor implements Deconstructor
{
    public function one(string $item): array
    {
        $oldArray = explode('.', $item);
        
        $string = $oldArray[0];

        $newArray = [];

        if (count($oldArray) > 1) {
            for ($i = 1; $i < count($oldArray); $i++) {
                array_push($newArray, $string . '.*');
                $string .= ".{$oldArray[$i]}";
            }
        }

        array_push($newArray, $string);

        return array_values(array_unique($newArray));
    }

    public function all(array $items): array
    {
        $newArray = [];

        foreach ($items as $item) {
            $newArray = array_merge($newArray, $this->one($item));
        }

        return array_values(array_unique($newArray));
    }
}
