# Bigmom Point

WARNING:
- This package is not yet mature. Please do not install in production environments. There's a good chance bugs and performance issues exist within this package.

Point does two main things: Add points, get points.
But first, you have to set up point templates and tags.

Tags puts limit on point templates. Point templates have two types: forever and today. These types refer to how the limits should apply to these point templates.

## Install

`composer require bigmom/point`
`php artisan vendor:publish`
`php artisan migrate`

If [bigmom/auth](https://packagist.org/packages/bigmom/auth) has not been published yet, please do so.

## Usage

### Management

Login through `/bigmom`.Create point templates first before adding or retrieving points.

### Add Point

`Bigmom\Point\Facades\AddPoint`

Points can be added in either sync or queue. This can be set in config, or in the method itself.

`add()`
Parameters:
- `ownerId` = required string.
- `template` = required string/integer. Will resolve template by finding id. If not exists, will find by template subject.
- `value` = optional integer. Value of points to be added. Default is `1`.
- `shouldQueue` = optional boolean. Default `config('point.add.queue')`. If false, adding points will run in sync. Otherwise, use queue.

`addSync()`
A convenience method for `add($ownerId, $template, $value, false)`.
Parameters:
- `ownerId` = required string.
- `template` = required string/integer. Will resolve template by finding id. If not exists, will find by template subject.
- `value` = optional integer. Value of points to be added. Default is `1`.

`addAsync()`
A convenience method for `add($ownerId, $template, $value, true)`.
Parameters:
- `ownerId` = required string.
- `template` = required string/integer. Will resolve template by finding id. If not exists, will find by template subject.
- `value` = optional integer. Value of points to be added. Default is `1`.

### Get Point

`Bigmom\Point\Facades\GetPoint`

Get points. Uses builder pattern. i.e. `GetPoint::owner($ownerId)->subject($subject)->today()->get();`

- `owner($ownerId)` - Set owner ID to query.
- `subject($subject)` - Set point template subject to query.
- `from($from)` - Query only from this timestamp.
- `until($until)` - Query only until this timestamp.
- `date($date)` - Convenience method for query records made on a certain date.
- `today()` - Convenience method for querying records made today.
- `tags($tags)` - Query only point templates with given tags. Must be array or string in array form.
- `compact()` - Does not return point models, only total and limited points and number of records.
- `get()` - Use at the end. Returns total and limited points, number of records, and point models if compact() is not set. 